//
//  Constant.h
//  UnNote
//
//  Created by qingfeng on 17/03/18.
//  Copyright © 2017年 qingfeng. All rights reserved.
//

#import <Foundation/Foundation.h>


#define KEY_FIRTS_TIME @"FIRST_TIME"
#define KEY_SIGN_STATE @"SIGN_STATE"

//用户状态
#define SIGN_STATE_ON @"SIGN_STATE_ON"
#define SIGN_STATE_OFF @"SIGN_STATE_OFF"

//用户信息标志
#define USER_INFO_NAME @"USER_INFO_NAME"
#define USER_INFO_ID @"USER_INFO_ID"

@interface Constant : NSObject

@end
