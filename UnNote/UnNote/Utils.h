//
//  Utils.h
//  UnNote
//
//  Created by qingfeng on 17/03/18.
//  Copyright © 2017年 qingfeng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Utils : NSObject

+ (void)jumpToViewController:(NSString *)viewControllerIndentifier
       contextViewController:(UIViewController *)contextViewController
                     handler:(void (^)(void)) handler;

+ (NSString *)md5:(NSString *)str;

@end
