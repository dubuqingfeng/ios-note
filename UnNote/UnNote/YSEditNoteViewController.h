//
//  EditNoteViewController.h
//  UnNote
//
//  Created by qingfeng on 17/03/18.
//  Copyright © 2017年 qingfeng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YSNote.h"

@interface YSEditNoteViewController : UIViewController

@property (weak, nonatomic) YSNote *note;
@property (assign) bool isAdd;

@end
