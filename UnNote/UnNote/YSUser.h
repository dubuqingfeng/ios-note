//
//  YSUser.h
//  UnNote
//
//  Created by qingfeng on 15/12/27.
//  Copyright © 2017年 qingfeng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YSUser : NSObject

@property (copy, nonatomic) NSString *phoneNumber;
@property (copy, nonatomic) NSString *password;

@end
