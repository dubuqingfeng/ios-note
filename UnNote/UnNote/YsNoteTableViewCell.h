//
//  YsNoteTableViewCell.h
//  UnNote
//
//  Created by qingfeng on 15/12/27.
//  Copyright © 2017年 qingfeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YSNoteTableViewCell : UITableViewCell

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *title;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *content;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *date;

@end
