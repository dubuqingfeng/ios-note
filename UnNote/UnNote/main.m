//
//  main.m
//  UnNote
//
//  Created by qingfeng on 17/03/18.
//  Copyright © 2017年 qingfeng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
